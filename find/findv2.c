#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>

int isDirectory(char *fileName){
	printf("%s\n", fileName);
	struct stat info;
	int rv = lstat(fileName,&info);
	if(rv==-1){
		perror("stat failed");
		exit(1);
	}
	int mode=info.st_mode;
	if ((mode &  0170000) == 0040000) 
	 	return 1;
    return 0;
}

void find(char * path, char *fileType){
	struct dirent *entry;
	DIR *dp = opendir(path);
	int count=0;

	if(dp==NULL){
		//printf("%s\n", path);
		fprintf(stderr, "Cannot open directory:%s\n",path);
		return;
	} 
	while((entry = readdir(dp)) != NULL){
	   	if(entry == NULL && errno != 0){
	        perror("readdir failed");
	        exit(1);
	    } 

	    //else {
	       	





	     //   	if(strcmp(entry->d_name,fileName)==0){
	     //   		printf("%s/%s\n", path,fileName);
	     //   		break;		           	
	   		// }

	   		//else {

	   			if(strcmp(entry->d_name,".")==0 || strcmp(entry->d_name,"..")==0){
	   				continue;
	   			}
	   			
	   			int i=0,j=0;
	   			char temp[100]={'\0'};

	   			if(path[0]=='/') { //absolutePath
	    			
	    			while(path[i]!='\0'){
	    				temp[i]=path[i];
	    				i++;
	    			}
	    			if(temp[i-1]!='/'){
	    				temp[i]='/';	    			
	    				i++;
					}	    				
	    		}
	    		while(entry->d_name[j]!='\0'){
	    			temp[i]=entry->d_name[j];
	    			i++;
	    			j++;
	    		}

	   			struct stat info;
				int rv = lstat(temp,&info);
				if(rv==-1){
					continue;
					//perror("stat failed");
					//exit(1);
				}
				int mode=info.st_mode;

				char getType;
				if ((mode &  0170000) == 0010000) 
	 				getType='p';
    			else if ((mode &  0170000) == 0020000) 
	 				getType='c';
    			else if ((mode &  0170000) == 0040000) 
	 				getType='d';
    			else if ((mode &  0170000) == 0060000) 
					getType='b';
    			else if ((mode &  0170000) == 0100000) 
					getType='-';
    			else if ((mode &  0170000) == 0120000) 
					getType='l';
    			else if ((mode &  0170000) == 0140000) 
					getType='s';
    			else 
					getType='0';


				if(fileType[0]==getType){
					printf("%s\n", temp);
	        		continue;
				}




				if ((mode &  0170000) == 0040000){
					int k=0,l=0;
		   			char ch[1000]="\0";
		   			while(path[k]!='\0'){
		   				ch[k]=path[k];
		   				k++;
		   			}
		   			ch[k]='/';
		   			k++;
		   			while(entry->d_name[l]!='\0'){
	    				ch[k]=entry->d_name[l];
	    				k++;l++;
	    			}
	    			//printf("%s\n",ch );
		   			find(ch,fileType);
	   			}
				

	   		//}
	   		
	    //}
	}
}

int main(int argc, char *argv[]){
	
	if(argc<4){
		printf("Invalid Command\n");
	} else {
	
		find(argv[1],argv[3]);
	}



	return 0;
}