#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>

extern int errno;

void ls_command(char *path){

	struct dirent *entry;
	DIR *dp = opendir(path);

	if(dp==NULL){
		fprintf(stderr, "Cannot open directory:%s\n",path);
		return;
	} else {
		while((entry = readdir(dp)) != NULL){
	        if(entry == NULL && errno != 0){
	            perror("readdir failed");
	            exit(1);
	        }else {
	             printf("%s\n",entry->d_name);
	   		}
	   	}
	}
	closedir(dp);
}


int main(int argc, char * argv[]){
	if(argc==1){
		ls_command(".");
	} else {
		for(int i=1;i<argc;i++){
			printf("%s:\n",argv[i]);
			ls_command(argv[i]);
			printf("\n");
		}
	}

	return 0;

}
