#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>

extern int errno;

void ls_command(char *path){

	struct dirent *entry;
	DIR *dp = opendir(path);
	int count=0;



	if(dp==NULL){
		fprintf(stderr, "Cannot open directory:%s\n",path);
		return;
	} 
	//counting the number of dirs/files into the directory
	while((entry = readdir(dp)) != NULL){
	   	if(entry == NULL && errno != 0){
	        perror("readdir failed");
	        exit(1);
	    }else {
	       	if(entry->d_name[0]!='.') 
	           	count++;
	   	}
	}
	rewinddir(dp);
	char *filesList[count];

	//putting the file into the array
	int index=0;
	while((entry=readdir(dp))!=NULL){
		//printf("Hellos\n");
		if(entry==NULL && errno!=0){
			perror("readdir failed");
			exit(1);
		} else {
			if(entry->d_name[0]!='.'){
				filesList[index]=(char*) malloc (strlen(entry->d_name)+1);
				strncpy (filesList[index],entry->d_name,strlen(entry->d_name));
				index++;
			}
		}
	}



	//sorting array
	char temp[100];
	for (int i = 0; i < index - 1 ; i++){
        for (int j = i + 1; j < index; j++){
            if (strcmp(filesList[i], filesList[j]) > 0){
                    strcpy(temp, filesList[i]);
                    strcpy(filesList[i], filesList[j]);
                    strcpy(filesList[j], temp);
            }
            
        }

   	}

	for (int i = 0; i < index; i++)
		printf("%s\n", filesList[i]);	
	

	closedir(dp);
}


int main(int argc, char * argv[]){
	if(argc==1){
		ls_command(".");
	} else {
		for(int i=1;i<argc;i++){
			printf("%s:\n",argv[i]);
			ls_command(argv[i]);
			printf("\n");
		}
	}

	return 0;

}
