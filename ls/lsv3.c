#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>


extern int errno;

void showStats(char *fileName){
	
	
	//printf("%s\n", fileName);

	struct stat info;
	int rv = lstat(fileName,&info);
	if(rv==-1){
		perror("stat failed");
		exit(1);
	}

	printf("%7ld",info.st_ino);

	//printf("mode: %o\n",info.st_mode);
	int mode=info.st_mode;
	char str[11];
	strcpy(str, "----------");

	//owner  permissions
    if((mode & 0000400) == 0000400) str[1] = 'r';
    if((mode & 0000200) == 0000200) str[2] = 'w';
    if((mode & 0000100) == 0000100) str[3] = 'x';
	//group permissions
    if((mode & 0000040) == 0000040) str[4] = 'r';
    if((mode & 0000020) == 0000020) str[5] = 'w';
    if((mode & 0000010) == 0000010) str[6] = 'x';
	//others  permissions
    if((mode & 0000004) == 0000004) str[7] = 'r';
    if((mode & 0000002) == 0000002) str[8] = 'w';
    if((mode & 0000001) == 0000001) str[9] = 'x';
	//special  permissions
    if((mode & 0004000) == 0004000) str[3] = 's';
    if((mode & 0002000) == 0002000) str[6] = 's';
    if((mode & 0001000) == 0001000) str[9] = 't';
   
    if ((mode &  0170000) == 0010000) 
	 	str[0]='p';
    else if ((mode &  0170000) == 0020000) 
	 	str[0]='c';
    else if ((mode &  0170000) == 0040000) 
	 	str[0]='d';
    else if ((mode &  0170000) == 0060000) 
		str[0]='b';
    else if ((mode &  0170000) == 0100000) 
		str[0]='-';
    else if ((mode &  0170000) == 0120000) 
		str[0]='l';
    else if ((mode &  0170000) == 0140000) 
		str[0]='s';
    else 
		str[0]='0';






    printf(" %s", str);

	printf(" %3ld",info.st_nlink);

	struct passwd *passwdInfo=getpwuid(info.st_uid);
	printf(" %s",passwdInfo->pw_name);

	struct group *groupInfo = getgrgid(info.st_gid);
	printf(" %s",groupInfo->gr_name);

	printf(" %5ld ",info.st_size);

	long secs = info.st_mtime;
	char *ch = ctime(&secs);
	for(int i=4;i<strlen(ch)-9;i++)
		printf("%c", ch[i]);
	printf(" ");

	
	if ( mode & S_IXUSR)
    	printf("\033[1;32m");
	if ((mode &  0170000) == 0040000) 
	    printf("\033[1;34m");
	if ((mode &  170000) == 120000)
	    printf("\033[1;36m");
	int u=0;
	while(fileName[u]!='\0'){
	    if(fileName[u]=='.' && fileName[u+1]=='t' && fileName[u+2]=='a' && fileName[u+3]=='r' || 
	       	fileName[u]=='.' && fileName[u+1]=='g' && fileName[u+2]=='z' || fileName[u]=='.' && fileName[u+1]=='z' && fileName[u+2]=='i' && fileName[u+3]=='p'){
	        
	        printf("\033[1;31m");
	        break;
	    }
	    u++;
	}
	u=0;
	while(fileName[u]!='\0'){
	    if(fileName[u]=='.' && fileName[u+1]=='j' && fileName[u+2]=='p' && fileName[u+3]=='g' || fileName[u]=='.' && fileName[u+1]=='p' && fileName[u+2]=='n' && fileName[u+3]=='g' || fileName[u]=='.' && fileName[u+1]=='j' && fileName[u+2]=='p' && fileName[u+3]=='e' &&fileName[u+4]=='g'){
	        printf("\033[1;35m");
	        break;
	    }
	    u++;
	}

	if((mode & 00000002) == 00000002 && (mode &  0170000) == 0040000) {
	    printf("\033[1;42m%s\033[0m\n",fileName);
	    printf("\033[0m");
	}

	else {
	    printf("%s\n", fileName );
	}
	printf("\033[0m");

	//printf(" %s\n", fileName);
}

void ls_command(char *path){

	struct dirent *entry;
	DIR *dp = opendir(path);
	int count=0;

	if(dp==NULL){
		fprintf(stderr, "Cannot open directory:%s\n",path);
		return;
	} 
	//counting the number of dirs/files into the directory
	while((entry = readdir(dp)) != NULL){
	   	if(entry == NULL && errno != 0){
	        perror("readdir failed");
	        exit(1);
	    }else {
	       	if(entry->d_name[0]!='.') 
	           	count++;
	   	}
	}
	rewinddir(dp);
	char *filesList[count];

	//putting the file into the array
	int index=0;
	while((entry=readdir(dp))!=NULL){
		if(entry==NULL && errno!=0){
			perror("readdir failed");
			exit(1);
		} else {
			if(entry->d_name[0]!='.'){
				filesList[index]=(char*) malloc (strlen(entry->d_name)+1);
				strncpy (filesList[index],entry->d_name,strlen(entry->d_name));
				index++;
			}
		}
	}

	//sorting array
	char temp[100];
	for (int i = 0; i < index - 1 ; i++){
        for (int j = i + 1; j < index; j++){
            if (strcmp(filesList[i], filesList[j]) > 0){
                    strcpy(temp, filesList[i]);
                    strcpy(filesList[i], filesList[j]);
                    strcpy(filesList[j], temp);
            }
            
        }

   	}

	for (int i = 0; i < index; i++) {
		//printf("%s\n", filesList[i]);

		if(strcmp(path,".")==0){
			showStats(filesList[i]);
			continue;
		}
		char temp[1000]={'\0'};
		int j=0;
		while(path[j]!='\0'){
			temp[j]=path[j];
			j++;
		}
		if(j!=1){
			temp[j]='/';
			j++;
		}
		
		int index=0;
		while(filesList[i][index]!='\0'){
			temp[j]=filesList[i][index];
			j++;
			index++;
		}

		showStats(temp);			
	}

	closedir(dp);
}



int main(int argc, char * argv[]){
	if(argc==1){
		ls_command(".");
	} else {
		for(int i=1;i<argc;i++){
			printf("%s:\n",argv[i]);
			ls_command(argv[i]);
			printf("\n");
		}
	}

	return 0;

}
