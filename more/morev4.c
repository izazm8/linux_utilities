#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/wait.h>


int PAGELEN=20;
#define	LINELEN	512
float count=0.0;

void do_more(FILE *);
int  get_input(FILE*, int);

int main(int argc, char *argv[]){

	if(argc==1){
		do_more(stdin);
	} else {
		FILE *fp;

      for(int i=1;i<argc;i++) {
         char ch;
         fp = fopen(argv[i] , "r");
            if (fp == NULL){
               perror("Can't open file");
               exit (1);
            } else {
               ch=getc(fp);
               while(ch !=EOF) {
                  if(ch=='\n')
                     count++;
                  ch=getc(fp);
               }
            }
            fclose(fp);
      }

		for(int i=1;i<argc;i++) {
			fp = fopen(argv[i] , "r");
      		if (fp == NULL){
         		perror("Can't open file");
         		exit (1);
      		}
      		do_more(fp);
      		fclose(fp);
		}
	}

	return 0;
}


void do_more(FILE *fp){

   struct termios old_tio, new_tio;
   int res=0;
/* get the terminal settings for stdin */
   res=tcgetattr(STDIN_FILENO, &old_tio);
/* we want to keep the old setting to restore them a the end */
   memcpy(&new_tio, &old_tio , sizeof(new_tio));
/* disable canonical mode (buffered i/o) and local echo */
   new_tio.c_lflag &=(~ICANON );/*& ~ECHOE  );*/
/* set the new settings immediately */
   tcsetattr(STDIN_FILENO,TCSANOW,&new_tio);

   int lineCount=0;  //because num_of_lines is decremented in the loop.
   int num_of_lines = 0;
   int rv;
   char buffer[LINELEN];
   char searchString[1000];
   char prev[1000],prev1[1000];
   FILE* fp_tty = fopen("/dev//tty", "r");
   while (fgets(buffer, LINELEN, fp)){
      fputs(buffer, stdout);
      num_of_lines++;
      lineCount++;
      if (num_of_lines >= PAGELEN){
         rv = get_input(fp_tty,lineCount);		
         if (rv == 0){//user pressed q
            printf("\033[1A \033[2K \033[1G");
            break;//
         }
         else if (rv == 1){//user pressed space bar
            printf("\033[1A \033[2K \033[1G");
            num_of_lines -= PAGELEN;
         }
         else if (rv == 2){//user pressed return/enter
            printf("\033[1A \033[2K \033[1G");
	         num_of_lines -= 1; //show one more line
         }
         else if (rv == 3){ //invalid character
            printf("\033[1A \033[2K \033[1G");
            break; 
         } else if (rv==4){
            printf(" \033[2K \033[1G");
            printf("/");
            fgets(searchString,100,stdin);

            while(fgets(buffer,LINELEN,fp)) {

               if(strcmp(buffer,searchString)==0){
                  printf("...../skipping\n");
                  printf("%s",prev );
                  printf("%s",prev1 );
                  printf("%s\n", searchString);
                  break;
               }
               
               strcpy(prev,buffer);
               strcpy(prev1,prev);
               lineCount++;
            }
            num_of_lines=3;
         }
      }
  }

}

int get_input(FILE* cmdstream, int num_of_lines)
{
   int c;

   struct winsize ws;
   ioctl(2,TIOCGWINSZ,&ws);
   PAGELEN=ws.ws_row-1;

   float avg=(num_of_lines/count)*100;
   printf("\033[7m --more--(%0.2f%%) \033[m",avg);
   c = getc(cmdstream);
   if(c == 'q')
	  return 0;
   if ( c == ' ' )			
	  return 1;
   if ( c == '\n' )	
	  return 2;
   if (c=='/'){
      return 4;
   }
   return 3;
}
